## Written by
- Hew Li Wen Galen
- galenhew@gmail.com

## 1. Overview of the submitted folder and the folder structure.
- The src folder consists of machine learning pipeline for predicting the factors that could influence student grades. 
- The EDA ipynb file explores the data. You may run it on a jupyter notebook
- User run.sh to run the machine learning pipeline

## 2. Instructions for executing the pipeline and modifying any parameters.
- pip install requirements.txt
- bash run.sh

## 3. Description of logical steps/flow of the pipeline. If you find it useful, please feel free to include suitable visualization aids (eg, flow charts) within the README.
- The main_pipeline runs preprocess functions, then runs the ml_func.
- The predicted output is given in prediction sample, and metrics as well.

## 4. Overview of key findings from the EDA conducted in Task 1 and the choices made in the pipeline based on these findings, particularly any feature engineering. Please keep the details of the EDA in the .ipynb, this section should be a quick summary.
- The EDA revealed that we had to remove duplicate rows, outliers and several variables did not have any impact on the response variable. Further, we could see that missing values had to be imputed in for a couple variables.

## 5. Explanation of your choice of models for each machine learning task.
- Median predicition was chosen as the naive baseline prediction.
- Linear regression was chosen first as baseline regression prediction too.
- KNN was chosen for classification as the sample size was below 50000 thus it would not take too long to run, and performs relatively well.

## 6. Evaluation of the models developed. Any metrics used in the evaluation should also be explained.
- The models performed better for classification than regression, with accuracy of 60%. The data was difficult to normalize to gaussian and thus classification increase degrees of freedom for the model to perform better. Cross validation is used to compare across models.
- ![cross_val](./data/cross_val.png)
- Using classfication model makes more sense as well since the objective is to find out overall whether students would underperform due to characteristics, and not to predict their exact scores.  
- RMSE is used instead of MSE for dealing with large error values, while MAE deals with smaller errors and thus having these measures enable us to have a comprehensive view.
- Gradient boosted regression and random forest perfomed best with lowest MAE and RMSE scores, which is expected.

## 7. Other considerations for deploying the models developed.
- Using sklearn pipeline module would make the pipeline more scalable
- Using argparse functions would make it more flexible for users
- Adding tests would help us pick up errors i.e. for missing values after preprocessing and transformations