import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import sqlite3

from sklearn.preprocessing import PowerTransformer
from sklearn.metrics import mean_absolute_error
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression

from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import IterativeImputer
from sklearn.impute import KNNImputer

from sklearn.neighbors import KNeighborsClassifier
from sklearn import metrics

from sklearn.linear_model import LinearRegression
from sklearn.linear_model import ElasticNet
from sklearn.ensemble import RandomForestRegressor
from sklearn.ensemble import ExtraTreesRegressor
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.svm import SVR

from sklearn.model_selection import cross_val_score


import preprocess
import ml_func

sql_conn = sqlite3.connect('./data/score.db')
raw_data= pd.read_sql_query('SELECT * FROM score ', sql_conn)


# -----------------global variables-----------------
SEED = 42

df= raw_data.copy() 
df= df.drop('index', 1)

# -----------------preprocess-----------------
df = preprocess.process(df)

pt = PowerTransformer(method='yeo-johnson', standardize=True,) 
df[['final_test',
'attendance_rate', 'n_female', 
'hours_per_week','sleep_hours']] = pt.fit_transform(df[['final_test', 'attendance_rate', 
                                                'n_female', 'hours_per_week','sleep_hours']])

# split data
X = df.drop(['final_test', 'final_test_bins'],1)
y = df['final_test']
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state= SEED)


# -----------------model-----------------

# -----------------regression-----------------
# Names of models
model_name_list = ['Linear Regression', 'ElasticNet Regression',
                  'Random Forest', 'Extra Trees', 'SVM',
                    'Gradient Boosted', 'Baseline']

# Instantiate the models
model1 = LinearRegression()
model2 = ElasticNet(alpha=1.0, l1_ratio=0.5)
model3 = RandomForestRegressor(n_estimators=100)
model4 = ExtraTreesRegressor(n_estimators=100)
model5 = SVR(kernel='rbf', degree=3, C=1.0, gamma='auto')
model6 = GradientBoostingRegressor(n_estimators=50)

# run models
model_list= [model1, model2, model3, model4, model5, model6 ]
regression = ml_func.train_model(model_name_list, model_list, X_train, y_train, X_test, y_test, pt)


#  -----------------classification models-----------------

X_class = df.drop(['final_test', 'final_test_bins'],1)
y_class = df['final_test_bins']

X_train_class, X_test_class, y_train_class, y_test_class = train_test_split(X_class, y_class, test_size=0.2, random_state= SEED)

knn = KNeighborsClassifier(n_neighbors=5)
knn.fit(X_train_class, y_train_class)
y_pred = knn.predict(X_test_class)


# metrics
scores = cross_val_score(knn, X_class, y_class, cv=10, scoring='accuracy')
# search for an optimal value of K for KNN
k_range = list(range(1, 31))
k_scores = []
for k in k_range:
    knn = KNeighborsClassifier(n_neighbors=k)
    scores = cross_val_score(knn, X_class, y_class, cv=10, scoring='accuracy')
    k_scores.append(scores.mean())

# plot the value of K for KNN (x-axis) versus the cross-validated accuracy (y-axis)
plt.plot(k_range, k_scores)
plt.xlabel('Value of K for KNN')
plt.ylabel('Cross-Validated Accuracy')


def main():
    print(regression)
    print(k_scores)
    



if __name__ == '__main__':
    main()