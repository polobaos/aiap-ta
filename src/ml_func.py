import pandas as pd
import numpy as np

def y_pred_sample(model, y_prediction, X_test, pt):
  # y_sample = model.predict(y_prediction.iloc[:1])
  y_sample= y_prediction.reshape(-1,1)

  # require all transformed variables to inverse transform
  df= pd.DataFrame(X_test)
  df1= df[['attendance_rate', 'n_female', 'hours_per_week','sleep_hours']]
  df1.insert(loc=0, column= 'final_test', value = y_sample)
  y_output=pt.inverse_transform(df1)
  return y_output[0][0]



def train_model( model_name_list, model_list, X_train, y_train, X_test, y_test, pt):
  results = pd.DataFrame(columns=['mae', 'rmse', 'prediction sample'], index = model_name_list)
  # Train and predict with each model
  for i, model in enumerate(model_list):
      model.fit(X_train, y_train)
      predictions = model.predict(X_test)
      y_output_sample= y_pred_sample(model, predictions, X_test, pt)

      # Metrics
      mae = np.mean(abs(predictions - y_test))
      rmse = np.sqrt(np.mean((predictions - y_test) ** 2))
      
      # Insert results into the dataframe
      model_name = model_name_list[i]
      results.loc[model_name, :] = [mae, rmse, y_output_sample]

  # Median Value Baseline Metrics
  baseline = np.median(y_train)
  baseline_mae = np.mean(abs(baseline - y_test))
  baseline_rmse = np.sqrt(np.mean((baseline - y_test) ** 2))

  results.loc['Baseline', :] = [baseline_mae, baseline_rmse, 'na']

  return results