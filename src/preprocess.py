import pandas as pd
import numpy as np

from sklearn.preprocessing import PowerTransformer
import util_func

# ------treat missing values------
def process(df):
    df= util_func.impute(df, ['attendance_rate','final_test'])


    # ------standardize categories------
    df.loc[(df['tuition']== 'Y'), 'tuition'] = 'Yes'
    df.loc[df['tuition']== 'N', 'tuition'] = 'No'
    df['CCA']= df['CCA'].str.lower()

    # ------convert to category------
    categories= df.select_dtypes(include=['object']).columns.tolist()
    categories.remove('sleep_time')
    categories.remove('wake_time')
    for col in categories:
        df[col] = df[col].astype('category')

    # ------drop duplicates------
    df= df.drop_duplicates()
    df= df.drop_duplicates('student_id')

    # ------treat outliers------
    df = df.loc[df['age'] > 7]

    #------create features------
    ## create sleeping hours
    df['sleep_time2']= np.where( pd.to_datetime(df['sleep_time']).dt.hour.between(17,23), 
                                pd.to_datetime(df['sleep_time']), pd.to_datetime(df['sleep_time']) +pd.offsets.Day(1) )
    df['wake_time2'] = pd.to_datetime(df['wake_time'])+ pd.offsets.Day(1)
    df['sleep_hours'] = (df['wake_time2'] - df['sleep_time2']).astype('timedelta64[h]')


    # create binned response variable
    df_bins = pd.qcut(df['final_test'], q=4).value_counts()
    df['final_test_bins']= pd.qcut(df['final_test'], q=4, labels= False)

    # ------remove features that have no effect------
    ## remove n_male as correlation with n_female is -1
    df=df.drop(['wake_time','sleep_time','sleep_time2', 'wake_time2','gender','age',
                'mode_of_transport', 'bag_color', 'student_id', 'n_male'], 1)


    # ------encode categorical features------

    encode_categories= df.select_dtypes(include=['category']).columns.tolist()

    df = pd.get_dummies(data = df, prefix = encode_categories, prefix_sep='_',
                columns = encode_categories,
                drop_first =False,
                dtype='int8')

    return df
    

def main():
    pass


if __name__ == '__main__':
    main()