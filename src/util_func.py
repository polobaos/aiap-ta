import pandas as pd
import numpy as np
from sklearn.impute import IterativeImputer


# ------treat missing values------
## ideally treat after split
def impute(df, col_name):
  df_impute = df.copy(deep=True)
  mice_imputer = IterativeImputer()
  for col in col_name:
    df_impute[col] = mice_imputer.fit_transform(df_impute[[col]])

  return df_impute



